# How to setup Windows Linux Subsystem (WSL) 2 for usage with Docker and Kubernetes

- [How to setup Windows Linux Subsystem (WSL) 2 for usage with Docker and Kubernetes](#how-to-setup-windows-linux-subsystem-wsl-2-for-usage-with-docker-and-kubernetes)
  - [Setup WLS 2](#setup-wls-2)
  - [Setup Docker](#setup-docker)
  - [Setup ssh](#setup-ssh)
  - [Setup git](#setup-git)
  - [Setup Visual Studio Code](#setup-visual-studio-code)
  - [Setup AWS CLI](#setup-aws-cli)
  - [Setup Kubernetes / Kind](#setup-kubernetes--kind)
  - [Setup aliases](#setup-aliases)

Goal: Setup a convenient UNIX-like work environment under Windows 10 that supports easy interaction with Docker, Kubernetes and Cloud Providers.


## Setup WLS 2 
- See https://docs.microsoft.com/en-us/windows/wsl/install-win10
- See Blogpost about setting up on WIN 10 WLS2, Docker, Kubernetes with KIND and Minikube https://kubernetes.io/blog/2020/05/21/wsl-docker-kubernetes-on-the-windows-desktop/
- Currently only possible if you have joined the Winodws Insider Programm. May not work if you have an enterprise Winodws Account.
PS C:\WINDOWS\system32> wsl --set-default-version 2
WSL 2 requires an update to its kernel component. For information please visit https://aka.ms/wsl2kernel
https://docs.microsoft.com/en-gb/windows/wsl/wsl2-kernel


## Setup Docker
- https://docs.docker.com/docker-for-windows/wsl/
- https://www.docker.com/blog/creating-the-best-linux-development-experience-on-windows-wsl-2/
- https://www.docker.com/blog/docker-desktop-wsl-2-best-practices/
## Setup ssh
- Generate ssh-keys
mkdir .ssh
chmod 600 .ssh
cd .ssh
ssh-keygen -t rsa -b 2048 -C "email@example.com"
https://docs.gitlab.com/ee/ssh/README.html#rsa-ssh-keys
If the key is not usable for some reason you may consult the permissions as in:
https://unix.stackexchange.com/questions/257590/ssh-key-permissions-chmod-settings
X. Setup ssh config
touch config
> If you do not have access from your default WSL Distro (e.g. Ubuntu) you may have to activate access via the Docker settings. Go to the Docker GUI Settings -> Settings -> WSL Integration -> Resources: Tick the "Enable integration with my default WSL distro" checkbox and "Apply & Restart".
## Setup git

> Further details may be found under: https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git

Set your name:
````
git config --global user.name "your_username"
````
Set your email:
````
git config --global user.email "your_email_address@example.com"
````
Check git config with: 
````
git config --global --list
````



## Setup Visual Studio Code
- Install Plugins

| Plugin              	| Purpose 	|
|---------------------	|---------	|
| Remote -WSL         	|         	|
| Kubernetes          	|         	|
| YAML                	|         	|
| Dokcker             	|         	|
| Markdown all in one 	|         	|
|                     	|         	|

## Setup AWS CLI
- https://docs.aws.amazon.com/cli/latest/userguide/install-windows.html


## Setup Kubernetes / Kind
- Kubectl aliases aka shortcuts https://github.com/ahmetb/kubectl-aliases


## Setup aliases
